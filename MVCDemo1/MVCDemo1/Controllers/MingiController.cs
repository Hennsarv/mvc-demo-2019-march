﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MVCDemo1.Controllers
{
    public class MingiController : Controller
    {
        // GET: Mingi
        public ActionResult Index(int? kordi, string id = "Henn")
        {
            string teade = $"{id} on tore poiss";
            ViewBag.kordi = kordi ?? 1;
            ViewBag.teade = teade;
            return View();
        }
    }
}