﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MVCDemo2.Models;

namespace MVCDemo2.Controllers
{
    public class InimeneController : Controller
    {
        // GET: Inimene
        public ActionResult Index()
        {

            return View(Inimene.Inimesed);
        }

        // GET: Inimene/Details/5
        public ActionResult Details(int id)
        {
            return View(Inimene.Get(id));
        }

        // GET: Inimene/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Inimene/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                Inimene inimene = new Inimene
                {
                    Nimi = collection["Nimi"],
                    Vanus = int.Parse(collection["Vanus"]),
                    Id = Inimene.Inimesed.Max(x => x.Id) +1
                };
                Inimene.Inimesed.Add(inimene);

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Inimene/Edit/5
        public ActionResult Edit(int id)
        {
            Inimene i = Inimene.Get(id);
            if (i != null) return View(i);
            else 
            return RedirectToAction("Index");
        }

        // POST: Inimene/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here
                Inimene i = Inimene.Get(id);
                if (i != null)
                {
                    i.Nimi = collection["Nimi"];
                    i.Vanus = int.Parse(collection["Vanus"]);
                }



                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Inimene/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Inimene/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
