﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MVCDemo2.Controllers
{
    public class UusController : Controller
    {
        // GET: Uus
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Tere(string nimi = "Henn")
        {
            ViewBag.teade = $"Tere {nimi}!";
            return View();
        }
    }
}