﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.ComponentModel.DataAnnotations;

namespace MVCDemo2.Models
{
    public class Inimene
    {
        public static List<Inimene> Inimesed = new List<Inimene>
        {
            new Inimene {Id = 1, Nimi = "Henn", Vanus = 64},
            new Inimene {Id = 2, Nimi = "Ants", Vanus = 40},
            new Inimene {Id = 3, Nimi = "Peeter", Vanus = 28}
        };

        // TODO: tee meetod inimese leidmiseks id järgi

        public static Inimene Get(int id)
        //=> Inimesed.Where(x => x.Id == id).SingleOrDefault();
        {
            foreach(Inimene x in Inimesed)
            {
                if (x.Id == id) return x;
            }
            return null;
        }

        [Key] public int Id { get; set; }
        public string Nimi { get; set; }
        public int Vanus { get; set; }

    }
}